package com.example.demo.model;

public class User {

	
	private final  Long id;
	private final String name;
	@SuppressWarnings("unused")
	private User(){
		this(null,null);
	}
	
	public User(final  Long id,final String name){
		this.id=id;
		this.name=name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	
}
