package com.example.demo.resource;



import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.User;


@RequestMapping(path="/mentorz")
public interface UserResource {
	
	@RequestMapping(path="/api/user",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	ResponseEntity<Object> register(@RequestBody User user, @RequestHeader(value="User-Agent") String userAgent);               //done
	
	@ResponseBody
	@RequestMapping(path="/api/user/id/{id}/users", method = RequestMethod.GET)
	void get(@RequestParam("name")  String name, @PathVariable(value="id") Integer id);            //

	@ResponseBody
	@RequestMapping(path="/api/user/name/{name}", method = RequestMethod.GET)
	void get( @PathVariable(value="name") String name,@RequestParam("address") String address);
	
	

}
