package com.example.demo.resource.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.resource.UserResource;
import com.example.demo.service.impl.UserServiceImpl;

@RestController
public class UserResourceImpl implements UserResource {

	
	@Autowired
    private UserServiceImpl userService;
	
	
	@Override
	@RequestMapping(path="api/user",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public ResponseEntity<Object> register(@RequestBody User user, @RequestHeader(value="User-Agent")  String userAgent) {
		System.out.println("id="+user.getId());
		System.out.println("name="+user.getName());
		System.out.println("header param -user agent="+userAgent);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}


	@Override
	public void get(String name,Integer id) {
	System.out.println("name=  "+name +"  id=  "+id);
       }
	
	
    @Override
	public void get(String name,String address) {
		System.out.println("  name=  "+name);
		System.out.println(" address=  "+address);
	}


}
