package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	
	@Override
	public void register(User user) {
		LOGGER.info("Spring Boot First project");
		LOGGER.info("user id= "+user.getId()+" user name=============== "+user.getName());
	}

	@Override
	public String get(String name) {
		LOGGER.info("get user name======================"+name);
		return name;
	}

	

}
