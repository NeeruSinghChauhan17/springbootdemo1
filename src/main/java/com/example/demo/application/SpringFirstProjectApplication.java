package com.example.demo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.example.demo.service.impl.UserServiceImpl;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.example.demo.resource",
        "com.example.demo.service"})
public class SpringFirstProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringFirstProjectApplication.class, args);
	}
}
